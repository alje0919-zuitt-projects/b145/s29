const express = require('express');

const app = express();

const port = 4000;

// middleware

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Mock database
let users = []



// routes/endpoint
// http://localhost:4000/
app.get("/hello", (req, res) => {
	res.send("Hello world /hello endpoint!")
});

// post
app.post("/hello", (req, res) => {
	// res.body contains the content/data of the request body
	// 
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


// Create a POST route to register a user
app.post("/signup", (req, res) => {
	console.log(req.body);
	// validation
	// if content of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password.
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered.`)
	} else{
		res.send("Please input both username and password")
	}
})


// This route expects to receive a PUT request at the URI "/change-password"
// put
// this will update the password or a user that matches the information provided in the client
app.put("/change-password", (req, res) => {

let message;

	for(let i = 0; i < users.length; i++){
		// if username provided in the postman/client matches with current object
		if(req.body.username === users[i].username){
			// change password of the user found in loop by client provided pw
			users[i].password = req.body.password

			// message response
			message = `User ${req.body.username}'s password has been updated`;

			// breaks out of the loop once a user matches the username provided
			break;
		} else{
			message = "User does not exist" 
		}
	}

	res.send(message);

})


// S29 ACTIVITY ===========================================
/*
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
*/
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage")
});

app.get("/users", (req, res) => {
	res.send(users)
});



app.delete("/delete-user", (req, res) => {
	let message;
	
	if(users.length != 0) {
		
		for(let i=0; i < users.length; i++){
			
			if(req.body.username == users[i].username){
				users.splice(users[i], 1);

				message = `User ${req.body.username} has been deleted.`;
				break;
			}
		}
	}else {
		message = "Database has zero record"
	}
})



// returns a msg to confirm that server is running in the terminal
app.listen(port, () => console.log(`server running at port: ${port}`))